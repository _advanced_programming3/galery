#include "DatabaseAccess.h"
#include "MyException.h"

bool DatabaseAccess::open()
{
	if (sqlite3_open("galleryDB.sqlite", &_db) != SQLITE_OK) {
		_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}
	std::cout << "success to open DB" << std::endl;
	return true;
}

void DatabaseAccess::close()
{
	sqlite3_close(_db);
	_db = nullptr;
}

void DatabaseAccess::clear()
{
	free(_db);
	free(_errMessage);
}

void DatabaseAccess::queries2(std::string msg, int (*callback)(void*, int, char**, char**), void* data)
{
	if (sqlite3_exec(_db, msg.c_str(), callback, data, &_errMessage) != SQLITE_OK)
	{
		throw MyException(_errMessage);
	}
}


const std::list<Album> DatabaseAccess::getAlbums()
{
	std::list<Album> albums;
	queries2("select user_id, name, creation_date from albums;", listAlbums, &albums);
	return albums;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	std::list<Album> albums;
	queries2("select user_id, name, creation_date from albums;", listAlbums, &albums);
	return albums;
}

void DatabaseAccess::createAlbum(const Album& album)
{
	queries2("INSERT INTO albums (NAME, creation_date, user_id) VALUES('" + album.getName() + "', '" + album.getCreationDate() + "', " + std::to_string(album.getOwnerId()) + ");", nullptr, nullptr);
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	queries2("delete from pictures where album_id == (select id from albums where name == '" + albumName + "');", nullptr, nullptr);
	queries2("delete from albums where name == '" + albumName + "' and user_id == " + std::to_string(userId) + ";", nullptr, nullptr);
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	bool flag = false;
	queries2("select * from albums where user_id == " + std::to_string(userId) + " and name = '" + albumName + "';", exists, &flag);
	return flag;
}

Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	Album flag;
	std::list<Picture> pictures;
	queries2("select user_id, name, creation_date from albums where name == '" + albumName + "';", getAlbum, &flag);
	queries2("select id, name, location, creation_date from pictures where album_id == (select id from albums where name == '" + flag.getName() + "' and user_id == " + std::to_string(flag.getOwnerId()) + ");", listPictures, &pictures);
	flag.setPictures(pictures);
	return flag;
}

void DatabaseAccess::closeAlbum(Album& pAlbum)
{
}

void DatabaseAccess::printAlbums()
{
	queries2("select * from albums;", printColumn, nullptr);
}

void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	queries2("INSERT INTO pictures (NAME, location, creation_date, album_id) VALUES ('" + picture.getName() + "', '" + picture.getPath() + "', '" + picture.getCreationDate() + "', (select id from albums where name == '" + albumName + "'));", nullptr, nullptr);
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	queries2("delete from tags where picture_id == (select id from pictures where name == '" + pictureName + "' and album_id == (select id from albums where name == '" + albumName + "'));", nullptr, nullptr);
	queries2("delete from pictures where name == '" + pictureName + "' and album_id == (select id from albums where name == '" + albumName + "');", nullptr, nullptr);
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	queries2("INSERT INTO tags (picture_id, user_id) VALUES ((select id from pictures where name == '" + pictureName + "' and album_id == (select id from albums where name == '" + albumName + "')), " + std::to_string(userId) + ");", nullptr, nullptr);
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	queries2("delete from tags where picture_id == (select id from pictures where name == '" + pictureName + "' and album_id == (select id from albums where name == '" + albumName + "')) and user_id == " + std::to_string(userId) + ";", nullptr, nullptr);
}

void DatabaseAccess::printUsers()
{
	queries2("select * from users;", printColumn, nullptr);
}

User DatabaseAccess::getUser(int userId)
{
	User user(34, "sd");
	queries2("select id, name from users where id = " + std::to_string(userId) + ";", getUsers, &user);
	return user;
}

void DatabaseAccess::createUser(User& user)
{
	queries2("INSERT INTO users (NAME) VALUES('" + user.getName() + "');", nullptr, nullptr);
}

void DatabaseAccess::deleteUser(const User& user)
{
	queries2("delete from users where id == " + std::to_string(user.getId()) + ";", nullptr, nullptr);
}

bool DatabaseAccess::doesUserExists(int userId)
{
	bool flag = false;
	queries2("select * from users where id == " + std::to_string(userId) + ";", exists, &flag);
	return flag;
}

int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	int result;
	queries2("select count(id) from albums where user_id == " + std::to_string(user.getId()) + ";", sum, &result);
	return result;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	int result;
	queries2("select count(distinct pictures.album_id) from tags inner join pictures on tags.picture_id = pictures.id where user_id == " + std::to_string(user.getId()) + ";", sum, &result);
	return result;
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	int result;
	queries2("select count(id) from tags where user_id == " + std::to_string(user.getId()) + ";", sum, &result);
	return result;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	float average;
	queries2("select (select count(id) from tags where user_id == " + std::to_string(user.getId()) + ")/(select count(distinct pictures.album_id) from tags inner join pictures on tags.picture_id = pictures.id where user_id == " + std::to_string(user.getId()) + ");", avg, &average);
	return average;
}

User DatabaseAccess::getTopTaggedUser()
{
	User user(78, "jn");
	queries2("select id, name from users where id == (select user_id from tags group by user_id order by count(*) desc limit 1);", getUsers, &user);
	return user;
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	Picture picture(78, "jn");
	queries2("select id, name, location, creation_date from pictures where id == (select picture_id from tags group by picture_id order by count(*) desc limit 1);", getPicture, &picture);
	return picture;
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	std::list<Picture> pictures;
	queries2("select pictures.id, pictures.name, picture.location, picture.creation_date from pictures inner join tags on pictures.id = tags.picture_id where tags.user_id == " + std::to_string(user.getId()) + ";", listPictures, &pictures);
	return pictures;
}

int avg(void* data, int argc, char** argv, char** azColName)
{
	*(float*)data = std::atof(argv[0]);
	return 0;
}

int sum(void* data, int argc, char** argv, char** azColName)
{
	*(int*)data = atoi(argv[0]);
	return 0;
}

int exists(void* data, int argc, char** argv, char** azColName)
{
	*(bool*)data = true;
	return 0;
}

int listPictures(void* data, int argc, char** argv, char** azColName)
{
	if (argc == 0)
	{
		return 0;
	}
	int id = atoi(argv[0]);
	std::string name(argv[1]);
	std::string path(argv[2]);
	std::string date(argv[3]);
	Picture newPicture(id, name, path, date);
	((std::list<Picture>*)data)->push_back(newPicture);
	return 0;
}

int listAlbums(void* data, int argc, char** argv, char** azColName)
{
	if (argc == 0)
	{
		return 0;
	}
	int userId = atoi(argv[0]);
	std::string name(argv[1]);
	std::string date(argv[2]);
	Album newAlbum(userId, name, date);
	((std::list<Album>*)data)->push_back(newAlbum);
	return 0;
}

int getAlbum(void* data, int argc, char** argv, char** azColName)
{
	if (argc == 0)
	{
		return 0;
	}
	int userId = atoi(argv[0]);
	std::string name(argv[1]);
	std::string date(argv[2]);
	Album newAlbum(userId, name, date);
	*(Album*)data = newAlbum;
	return 0;
}

int getUsers(void* data, int argc, char** argv, char** azColName)
{
	if (argc == 0)
	{
		return 0;
	}
	int userId = atoi(argv[0]);
	std::string name(argv[1]);

	(*(User*)data).setId(userId);
	(*(User*)data).setName(name);
	return 0;
}

int printColumn(void* data, int argc, char** argv, char** azColName)
{
	for (int i = 0; i < argc; i++)
	{
		std::cout << azColName[i] << " = " << argv[i] << " , ";
	}
	std::cout << std::endl;
	return 0;
}

int getPicture(void* data, int argc, char** argv, char** azColName)
{
	if (argc == 0)
	{
		return 0;
	}
	int userId = atoi(argv[0]);
	std::string name(argv[1]);
	std::string path(argv[2]);
	std::string date(argv[3]);

	(*(Picture*)data).setId(userId);
	(*(Picture*)data).setName(name);
	(*(Picture*)data).setPath(path);
	(*(Picture*)data).setCreationDate(date);
	return 0;
}
